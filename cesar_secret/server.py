from flask import Flask, render_template, request, redirect, url_for, flash
import enchant
d = enchant.Dict("en_US")

app = Flask(__name__)
app.secret_key = 'many random bytes'

@app.route('/')
def index():
    data=""
    context={
        'data':data,
    }
    return render_template('index.html', **context )

@app.route('/cifrar',methods=['POST','GET'])
def proc_cifrar():
    flash("Mensaje cifrado")
    if request.method == 'POST':
        mensaje = request.form['mensaje_form'].upper()
        num=int(request.form['k_form'])
        mc=cifrar(mensaje,num)
        data = "".join(mc)
        context={
            'data':data,
            'msj': mensaje,
            'k_num':num,
        }
    return render_template('index.html', **context )

def cifrar(mensaje, k):
    abc = {'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14,
           'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26}
    palabras = mensaje.split()
    mensaje_cifrado = []
    for palabra in palabras:
        for caracter in palabra:
            x = str(caracter)
            if x in abc:
                valor = abc[x]
                if((valor + k) > 26):
                    valor_nuevo = (valor + k) % 26
                else:
                    valor_nuevo = (valor + k)
                #print("Valor: {}, valor_nuevo: {}".format(valor,valor_nuevo))
                for kd, v in abc.items():
                    if v == valor_nuevo:
                        caracter_nuevo = kd
                #print("C_original: {}, C_nuevo: {}".format(caracter,caracter_nuevo))
                mensaje_cifrado.append(caracter_nuevo)
            else:
                mensaje_cifrado.append(x)
        mensaje_cifrado.append(" ")
    mensaje_cifrado.pop()
    return mensaje_cifrado

@app.route('/des')
def des():
    data=""
    context={
        'data':data,
    }
    return render_template('descifrar.html', **context )

@app.route('/descifrar',methods=['POST','GET'])
def proc_descifrar():
    flash("Mensaje procesado")
    if request.method == 'POST':
        mensaje = request.form['mensaje_form'].upper()
        data=descifrar(mensaje)

        porcentajes=check_words(data)
        context={
            'msj': mensaje,
            'data':data,
            'porc':porcentajes,
        }
    return render_template('descifrar.html', **context )

def descifrar(mensaje):
    abc = {'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14,
           'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26}
    
    palabras = mensaje.split()
    mensajes_des=[]
    for k in range(1,26):
        mensaje_descifrado=[]
        for palabra in palabras:
            for caracter in palabra:
                x = str(caracter)
                if x in abc:
                    valor=abc[x]
                    if((valor - k)<0):
                        valor_nuevo = (valor - k) % 26
                    elif((valor - k)==0):
                        valor_nuevo = 26
                    else:    
                        valor_nuevo = (valor - k)
                    #print("Valor de k: {}, Valor: {}, valor_nuevo: {}".format(k, valor,valor_nuevo))
                    for kd,v in abc.items():
                        if v==valor_nuevo:
                            caracter_nuevo = kd  
                    #print("C_original: {}, C_nuevo: {}".format(caracter,caracter_nuevo))  
                    mensaje_descifrado.append(caracter_nuevo)
                else:
                    mensaje_descifrado.append(x)
            mensaje_descifrado.append(" ")
        mensajes_des.append(mensaje_descifrado)
    
    mensajes_limpios=[]
    for m in mensajes_des:
        mensajes_limpios.append( "".join(m))

    

    return mensajes_limpios
    #return mensajes_completos

def check_words(menjs):

    cont=0
    menjs_new=[]
    for m in menjs:
        palabras = m.split()
        lon= len(palabras)
        print("Mensje m: {}, cantidad de palabras: {}".format(m,lon))
        for p in palabras:
            r=d.check(p)
            if r:
                cont+=1
        porcentaje=(cont*100)/lon
        print("palabras correctas: {}, %: {}".format(cont,porcentaje))
        menjs_new.append(porcentaje)
        cont=0
    return menjs_new

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0",port=8080)
