def descifrar(mensaje):
    abc = {'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14,
           'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26}
    
    palabras = mensaje.split()
    mensajes_des=[]
    for k in range(1,25):
        mensaje_descifrado=[]
        for palabra in palabras:
            for caracter in palabra:
                x = str(caracter)
                if x in abc:
                    valor=abc[x]
                    if((valor - k)<0):
                        valor_nuevo = (valor - k) % 26
                    elif((valor - k)==0):
                        valor_nuevo = 26
                    else:    
                        valor_nuevo = (valor - k)
                    #print("Valor de k: {}, Valor: {}, valor_nuevo: {}".format(k, valor,valor_nuevo))
                    for kd,v in abc.items():
                        if v==valor_nuevo:
                            caracter_nuevo = kd  
                    #print("C_original: {}, C_nuevo: {}".format(caracter,caracter_nuevo))  
                    mensaje_descifrado.append(caracter_nuevo)
                else:
                    mensaje_descifrado.append(x)
            mensaje_descifrado.append(" ")
        mensajes_des.append(mensaje_descifrado)
    return mensajes_des

if __name__ == "__main__":
    mensaje="KROD".upper()
    print("Mensaje: {}".format(mensaje))
    mc=descifrar(mensaje)
    #mc_c = "".join(mc)
    print(mc)