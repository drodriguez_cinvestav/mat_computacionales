from flask import Flask, render_template, request, redirect, url_for, flash
from flask_mysqldb import MySQL
import math
import numpy as np

app = Flask(__name__)
app.secret_key = 'many random bytes'

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'david1990'
app.config['MYSQL_DB'] = 'db_itinerarios'

mysql = MySQL(app)

@app.route('/')
def Index():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM estados e INNER JOIN ciudades c on e.id_estado=c.id_estado WHERE c.id_ciudad != 2322")
    ciudades = cur.fetchall()
    cur.close()

    cur = mysql.connection.cursor()
    cur.execute("SELECT * from estados e inner join ciudades c on e.id_estado=c.id_estado inner join itinerario i on c.id_ciudad=i.id_ciudad;")
    itinerario = cur.fetchall()
    cur.close()

    context={
        'ciudades':ciudades,
        'itinerario':itinerario,
    }
    return render_template('index.html', **context )


@app.route('/insert', methods = ['POST'])
def insert():
    if request.method == "POST":
        flash("Ciudad agregada con éxito")
        ciudad_seleccionada = request.form['ciudad']
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO itinerario (id_ciudad) VALUES ({})".format(int(ciudad_seleccionada)))
        mysql.connection.commit()
        return redirect(url_for('Index'))

@app.route('/delete/<string:id_data>', methods = ['GET'])
def delete(id_data):
    flash("Ciudad eliminadad del itinerario")
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM itinerario WHERE id_ciudad={}".format(id_data))
    mysql.connection.commit()
    return redirect(url_for('Index'))


@app.route('/procesar',methods=['POST','GET'])
def procesar():
    if request.method == 'POST':
        latitud = request.form['latitud']
        longitud = request.form['longitud']

        cur = mysql.connection.cursor()
        cur.execute(""" 
            UPDATE ciudades
            SET latitud = {}, longitud = {}
            WHERE id_ciudad=2322;
        """.format(latitud,longitud))
        mysql.connection.commit()

        algoritmo = int(request.form['algoritmo'])

        cur = mysql.connection.cursor()
        cur.execute("SELECT id_ciudad FROM itinerario")
        itinerario = [item[0] for item in cur.fetchall()]
        mysql.connection.commit()

        cur = mysql.connection.cursor()
        cur.execute("""
        SELECT c.id_ciudad,e.estado,c.ciudad,c.latitud,c.longitud 
        FROM estados e inner join ciudades c on e.id_estado=c.id_estado 
        order by c.id_ciudad;""")
        ciudades = list([item for item in cur.fetchall()])
        mysql.connection.commit()

        if  algoritmo==0: #Exhaustivo
            c0=[2322,'Origen','Origen',latitud,longitud]
            ci=c0[0]

            l_itinerario=len(itinerario)
            rutas = []
            for p in permutaciones(itinerario):
                p.insert(0,ci)
                p.insert(l_itinerario+1,ci)
                rutas.append(p)

            ciudades_iti=[]
            for i in itinerario:
                c=get_ciudad(i)
                ciudades_iti.append(c)
            
            long_itinerario=len(rutas[0])
            rutas = exhaustivo(rutas,long_itinerario,ciudades)

            rutas_n= np.array(rutas)
            minimo=min(rutas_n[:,long_itinerario])
            print("Esto es con np...: {}".format(rutas_n[:,long_itinerario]))
            context={
                'lat':latitud,
                'log':longitud,
                'alg':'Exhaustivo',
                'rutas':rutas,
                'ciudades':ciudades,
                'c_iti':ciudades_iti,
                'minimo':minimo,         
            }
            return render_template('resultado.html', **context)
        else: #Voraz
            itinerario.insert(0,2322)
            itinerario.append(2322)

            ciudades_iti=[]
            for i in itinerario:
                c=get_ciudad(i)
                ciudades_iti.append(c)

            respuesta = voraz(itinerario, ciudades)

            context={
                'alg':'Voraz',
                'respuesta':respuesta,
                'c_iti':ciudades_iti,
            }
            return render_template('resultado2.html', **context)
    else:
        return "Problema al procesar"

def voraz (itinerario1,ciudades):
    itinerario = itinerario1
    ruta = []
    ruta.append(itinerario[0])
    itinerario.remove(itinerario[0])

    print (itinerario)
    print (ruta)
    j=1
    d_ruta=0

    while len(itinerario)>1:
        for i in range(len(itinerario)-1):
            c1=ciudades[ruta[len(ruta)-1]-1]
            c2=ciudades[itinerario[i]-1]
            d=distancia(c1,c2)
            if i==0:
                d_minima=d
            if d_minima >= d:
                d_minima=d
                flag=i
            #print("La distancia de {} a {} es: {} kilometros".format(c1[2],c2[2],d))
            c_cercana=ciudades[itinerario[flag]-1]
        print("La ciudad mas cercana a {} es {} a {} kilometros".format(c1[2],c_cercana[2],d_minima))
        j=j+1
        d_ruta=d_ruta+d_minima
        ruta.append(itinerario[flag])
        itinerario.remove(itinerario[flag])

    c1=ciudades[ruta[len(ruta)-1]-1]
    c2=ciudades[itinerario[0]-1]
    d=distancia(c1,c2)
    d_ruta=d_ruta+d
    print("La ciudad mas cercana a {} es {} a {} kilometros".format(c1[2],c2[2],d))
    ruta.append(itinerario[0])
    itinerario.remove(itinerario[0])
    print(itinerario)
    print("La ruta sugerida es {} con un recorrido de {} kilometros".format(ruta,d_ruta))
    respuesta = [ruta,d_ruta]
    return respuesta

def get_ciudad(id_ciudad):
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM ciudades Where id_ciudad={}".format(id_ciudad))
    ciudad = [item for item in cur.fetchall()]
    mysql.connection.commit()
    return ciudad

def exhaustivo(rutas,l_itinerario,ciudades):
    sum_rutas=[]
    z = 0
    i = 0
    for r in rutas:
        sum_dis = 0
        print("Ruta {}".format(r))
        for j in range(0,l_itinerario-1):
            print("Valor de i={}, j:{}, j+1:{}".format(i,j,j+1))
            c1=ciudades[rutas[i][j]-1]
            c2 = ciudades[rutas[i][j+1]-1]
            print(c1)
            print(c2)
            d = distancia(c1, c2)
            print("Distancia: {}".format(d))
            sum_dis=sum_dis+d
        print("Para z: {}, Sum_Dis:{}".format(z,sum_dis))
        print("#"*50)
        sum_rutas.insert(z,sum_dis)
        r.append(sum_dis)
        #matrix.append([r,sum_dis])
        i=i+1
        z=z+1
    #print(matrix)
    return rutas

def distancia(c1, c2):
    #Radio de la Tierra en Km = 6371Km
    rad = 6371
    
    #Obetención de latitudes y longitudes de los lugares 
    #[0-ID, 1-Estado, 2-Ciudad, 3-Latitud, 4-Longitud]
    lat1=c1[3]
    lon1=c1[4]
    lat2=c2[3]
    lon2=c2[4]

    # Distancia entre latitudes y longitudes 
    dLat = math.radians(lat2 - lat1) 
    dLon = math.radians(lon2 - lon1)
    lat1 = math.radians(lat1) 
    lat2 = math.radians(lat2)

    # Formula 
    a = (pow(math.sin(dLat / 2), 2) + pow(math.sin(dLon / 2), 2) *
         math.cos(lat1) * math.cos(lat2))
    
    c = 2 * math.asin(math.sqrt(a))
    return rad * c

def permutaciones(lst):
    # Lista vacia -> sin permutaciones
    if len(lst) == 0:
        return []

    # Un elemento -> solo una permutación
    if len(lst) == 1:
        return [lst]

    l = []  # Lista vacia que almacenerá la permutación
    # Iterar la lista y obtener permutacion 
    for i in range(len(lst)):
       m = lst[i]
       remLst = lst[:i] + lst[i+1:]
       # Generando permutacion con el primer elmento
       for p in permutaciones(remLst):
           l.append([m] + p)
    return l

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0",port=8080)
