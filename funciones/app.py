from flask import Flask, render_template, request, redirect, url_for, flash

app = Flask(__name__)
app.secret_key = 'many random bytes'

@app.route('/')
def index():
    data=""
    context={
        'data':data,
    }
    return render_template('index.html', **context )

@app.route('/gen',methods=['POST','GET'])
def gen():
    flash("Input enviado")
    if request.method == 'POST':
        A = request.form['dominio']
        B = request.form['codominio']
        C = request.form['exp_in']
        S = int(request.form['seleccion'])

        A=A.split(",")
        B=B.split(",")
       

        print("A: {}, B:{}, C:{}".format(A,B,C))

        matriz_r=[]
        matriz_f=[]
        matriz_fi=[]
        matriz_fs=[]
        matriz_fb=[]
        relacion_e=[]

        x=producto_x(A,B)
        print("Los pares ordenados son: ", x)

        y=c_relaciones(x)
        matriz_r=relaciones(len(A),len(B),y,A,B)
        especial=0
        if S == 1:
            res=matriz_r
        elif S == 2:
            res= funciones(matriz_r)
        elif S == 3:
            aux=funciones(matriz_r)
            res= funciones_i(aux)
        elif S == 4:
            aux=funciones(matriz_r)
            res= funciones_s(aux)
        elif S == 5:
            aux=funciones(matriz_r)
            res= funciones_b(aux)
        else:
            res = r_especifica(C,y,matriz_r)
            especial=1
            if res[0] == '':
                res[0]=C


        context={
            'res':res,
            'especial':especial,
        }
    return render_template('index.html', **context )

def producto_x(A,B):
    lista=[]
    for a in A:
        for b in B:
            lista.append((a+','+b))
    return lista

def funciones(resultado):
    matriz=[]
    for i in range (len(resultado)):
        if resultado[i][2] == '1': 
            matriz.append(resultado[i])
    return(matriz)

def funciones_i(matriz_f):
    matriz=[]
    for i in range (len(matriz_f)):
        if matriz_f[i][3] == '1': 
            matriz.append(matriz_f[i])
    return(matriz)

def funciones_s(matriz_f):
    matriz=[]
    for i in range (len(matriz_f)):
        if matriz_f[i][4] == '1': 
            matriz.append(matriz_f[i])
    return(matriz)

def funciones_b(matriz_f):
    matriz=[]
    for i in range (len(matriz_f)):
        if matriz_f[i][5] == '1': 
            matriz.append(matriz_f[i])
    return(matriz)

def relaciones(a,b,y,A,B):
    cont1=cont2=cont3=0
    flag=0
    contador=0
    contador_c=[]
    resultado=[]

    for i in range(b):
        contador_c.append(0)

    for i in range (len(y)):
        for j in range (a):
            if a == len(y[i]) and A[j] == y[i][j][0]:
                flag=1
                contador += 1
            else:
                flag = 0
        if flag == 1 and contador % a == 0:
            #print(y[i],"es funcion")
            resultado.append([")(".join(y[i]),'1','1'])

            for k in range(a):
                for l in range(b):
                    if y[i][k][2]== B[l]:
                        contador_c[l]+=1
            for m in range(b):
                if contador_c[m] == 1:
                    cont1+=1
                if contador_c[m] < 2:
                    cont2+=1
                if contador_c [m] == 0:
                    cont3=1
            if cont2==b:
                #print("soy inyectiva")
                resultado[i].append('1')
            else:
                resultado[i].append('0')
            if cont3 != 1 :
                #print("soy sobreyectiva")
                resultado[i].append('1')
            else:
                resultado[i].append('0')
            if cont1==b:
                #print("soy biyectiva")
                resultado[i].append('1')
            else:
                resultado[i].append('0')


            cont1=cont2=cont3=0
            contador_c=[]
            for l in range(b):
                contador_c.append(0)
        else:
            resultado.append([")(".join(y[i]),'1','0','0','0','0'])
        contador = 0
    
    return(resultado)

def c_relaciones(x):
    if len(x) == 0:
        return [[]]
    r = c_relaciones(x[:-1])
    return r + [s + [x[-1]] for s in r]
    

def r_especifica(C,y,matriz_r):
    aux=[]
    lista=[]
    C=C.replace("(", "")
    C=C.replace(")", "")
    print(C)
    band=0

    for i in range(len(y)):
        aux.append(",".join(y[i]))
    for r in range(len(aux)):
        if C == aux[r]:
            band=r

    if band == 0:
        print("No es relacion")
        lista=['','0','0','0','0','0']
    else:
        print("Es relación",matriz_r[band])
        lista=matriz_r[band]
    return lista


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0",port=8082)