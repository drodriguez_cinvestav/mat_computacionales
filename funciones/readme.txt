Integrantes del equipo:
David Rodríguez Hernández
Raúl Giovanni Silva Gómez

URL:
IP proporcionada por DHCP de Cinvestav

http://148.247.204.217:8082/

Lenguajes de programación y/o tecnologías   
utilizadas para el desarrollo del proyecto.

1. Lenguaje de programación: Python version 3.7.1
2. Interfaz web: Framework Flask versión 1.1.1
Paquetes adicionales necesario por Flask: 
    a) autopep8==1.4.4
    b) Click==7.0
    c) dominate==2.4.0
    d) Flask-Bootstrap==3.3.7.1 e) itsdangerous==1.1.0
    f) Jinja2==2.10.3
    g) MarkupSafe==1.1.1 h) pycodestyle==2.5.0
    i) visitor==0.1.3
    j) Werkzeug==0.16.0
