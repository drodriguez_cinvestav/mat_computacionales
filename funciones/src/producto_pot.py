import math; 
  
def PowerSet(set,set_size): 
    pow_set_size = (int) (math.pow(2, set_size)); 
    counter = 0; 
    j = 0; 
    lst=[]
    for counter in range(0, pow_set_size): 
        aux=[]
        for j in range(0, set_size):  
            if((counter & (1 << j)) > 0): 
                #print(set[j],end=","); 
                aux.append(set[j])
        #print("{}".format(counter))        
        lst.append(aux)
    return lst

def producto_x(A,B):
    lista=[]
    for a in A:
        for b in B:
            lista.append(a+','+b)
    return lista    
  
if __name__ == "__main__":
    A=['x','y']
    B=['1','2']
    set=producto_x(A,B)
    z= len(set)
    relaciones = PowerSet(set, z); 
    for r in relaciones:
        print(r)