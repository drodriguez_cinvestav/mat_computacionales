def producto_x(A,B):
    lista=[]
    for a in A:
        for b in B:
            lista.append((a+','+b))
    return lista

def funciones(resultado):
    matriz=[]
    for i in range (len(resultado)):
        if resultado[i][2] == '1': 
            matriz.append(resultado[i])
    return(matriz)

def funciones_i(matriz_f):
    matriz=[]
    for i in range (len(matriz_f)):
        if matriz_f[i][3] == '1': 
            matriz.append(matriz_f[i])
    return(matriz)

def funciones_s(matriz_f):
    matriz=[]
    for i in range (len(matriz_f)):
        if matriz_f[i][4] == '1': 
            matriz.append(matriz_f[i])
    return(matriz)

def funciones_b(matriz_f):
    matriz=[]
    for i in range (len(matriz_f)):
        if matriz_f[i][5] == '1': 
            matriz.append(matriz_f[i])
    return(matriz)

def relaciones(a,b,y):
    cont1=cont2=cont3=0
    flag=0
    contador=0
    contador_c=[]
    resultado=[]

    for i in range(b):
        contador_c.append(0)

    for i in range (len(y)):
        for j in range (a):
            if a == len(y[i]) and A[j] == y[i][j][0]:
                flag=1
                contador += 1
            else:
                flag = 0
        if flag == 1 and contador % a == 0:
            #print(y[i],"es funcion")
            resultado.append([")(".join(y[i]),'1','1'])

            for k in range(a):
                for l in range(b):
                    if y[i][k][2]== B[l]:
                        contador_c[l]+=1
            for m in range(b):
                if contador_c[m] == 1:
                    cont1+=1
                if contador_c[m] < 2:
                    cont2+=1
                if contador_c [m] == 0:
                    cont3=1
            if cont2==b:
                #print("soy inyectiva")
                resultado[i].append('1')
            else:
                resultado[i].append('0')
            if cont3 != 1 :
                #print("soy sobreyectiva")
                resultado[i].append('1')
            else:
                resultado[i].append('0')
            if cont1==b:
                #print("soy biyectiva")
                resultado[i].append('1')
            else:
                resultado[i].append('0')


            cont1=cont2=cont3=0
            contador_c=[]
            for l in range(b):
                contador_c.append(0)
        else:
            resultado.append([")(".join(y[i]),'1','0','0','0','0'])
        contador = 0
    
    return(resultado)

def c_relaciones(x):
    #print(c)
    """Calcula y devuelve el conjunto potencia del
       conjunto c.
    """
    if len(x) == 0:
        return [[]]
    r = c_relaciones(x[:-1])
    return r + [s + [x[-1]] for s in r]
    #print(c)


if __name__ == "__main__":
    A=['x']
    B=['1','2']
    
    matriz_r=[]
    matriz_f=[]
    matriz_fi=[]
    matriz_fs=[]
    matriz_fb=[]
      
    x=producto_x(A,B)
    print("Los pares ordenados son: ", x)

    y=c_relaciones(x)
    matriz_r=relaciones(len(A),len(B),y)
   

    print("Imprime las {} posibles relaciones".format(2**(len(A)*len(B))))
    for i in range (len(matriz_r)):
        print(matriz_r[i])

    matriz_f=funciones(matriz_r)
    print("Imprime las que son funciones")
    for i in range(len(matriz_f)):
        print(matriz_f[i])

    matriz_fi=funciones_i(matriz_f)
    print("Imprime las que son funciones inyectivas")
    for i in range(len(matriz_fi)):
        print(matriz_fi[i])
    
    matriz_fs=funciones_s(matriz_f)
    print("Imprime las que son funciones suprayectivas")
    for i in range(len(matriz_fs)):
        print(matriz_fs[i])

    matriz_fb=funciones_b(matriz_f)
    print("Imprime las que son funciones biyectivas")
    for i in range(len(matriz_fb)):
        print(matriz_fb[i])
    


    

   
